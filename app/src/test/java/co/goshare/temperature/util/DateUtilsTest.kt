package co.goshare.temperature.util

import org.junit.Assert
import org.junit.Test
import java.util.*

class DateUtilsTest {
    @Test
    fun formatDate() {
        val date = Date(DATE_IN_MILLIS)
        DateUtils.timeZone = utcTimezone
        Assert.assertEquals("22/08/20 18:50", DateUtils.format(date))

        DateUtils.timeZone = brazilTimezone
        Assert.assertEquals("22/08/20 13:50", DateUtils.format(date))
    }

    @Test
    fun formatDate_nullDate() {
        val date: Date? = null
        val formatted = DateUtils.format(date)
        Assert.assertEquals("", formatted)
    }

    @Test
    fun formatCalendar() {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = DATE_IN_MILLIS

        DateUtils.timeZone = utcTimezone
        Assert.assertEquals("22/08/20 18:50", DateUtils.format(calendar))

        DateUtils.timeZone = brazilTimezone
        Assert.assertEquals("22/08/20 13:50", DateUtils.format(calendar))
    }

    @Test
    fun formatCalendar_nullCalendar() {
        val calendar: Calendar? = null
        val formatted = DateUtils.format(calendar)
        Assert.assertEquals("", formatted)
    }

    companion object {
        private const val DATE_IN_MILLIS = 1598122258889
        private val utcTimezone = TimeZone.getTimeZone("UTC")
        private val brazilTimezone = TimeZone.getTimeZone("Brazil/Acre")
    }
}