package co.goshare.temperature.viewmodel

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.goshare.temperature.R
import co.goshare.temperature.model.bean.User
import co.goshare.temperature.model.data.Preferences
import co.goshare.temperature.model.data.Repository
import co.goshare.temperature.model.event.RxBus
import co.goshare.temperature.model.event.RxEvent
import co.goshare.temperature.util.ResourceProvider
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserViewModel(resourceProvider: ResourceProvider) : ViewModel() {
    private val resourceUri = "android.resource://co.goshare.temperature/"
    private val defaultPhotoUri = Uri.parse("$resourceUri${R.drawable.nav_header_default_user}")
    private val defaultUserName = resourceProvider.getString(R.string.nav_header_not_logged)
    private val defaultUserEmail = ""
    val userName = MutableLiveData(defaultUserName)
    val userEmail = MutableLiveData(defaultUserEmail)
    val userPhotoUrl = MutableLiveData(defaultPhotoUri)
    val signInRequested = MutableLiveData(false)
    val loggedAccount = MutableLiveData("")
    var googleSignInClient: GoogleSignInClient? = null

    init {
        GlobalScope.launch(Dispatchers.IO) {
            val activeUser = Preferences.activeUser
            loggedAccount.postValue(activeUser)

            if (activeUser?.isNotEmpty() == true) {
                val user = Repository.getUserById(activeUser)
                user?.let { loadUserProperties(it) }
            }
        }
    }

    fun login() {
        signInRequested.postValue(true)
    }

    suspend fun logout() {
        googleSignInClient?.signOut()
        userName.postValue(defaultUserName)
        userEmail.postValue(defaultUserEmail)
        userPhotoUrl.postValue(defaultPhotoUri)
        withContext(Dispatchers.IO) {
            Preferences.activeUser = null

            loggedAccount.postValue("")
            RxBus.send(RxEvent.LoginStateChanged())
        }
    }

    fun isLogged() = !Preferences.activeUser.isNullOrEmpty()

    fun onSignInFlowStarted() {
        signInRequested.postValue(false)
    }

    fun onSignInResultReceived(completedTask: Task<GoogleSignInAccount>) {
        val account = completedTask.getResult(ApiException::class.java)
        account?.let {
            val name = requireNotNull(account.displayName)
            val email = requireNotNull(account.email)
            val photoUrl = account.photoUrl?.toString() ?: ""

            GlobalScope.launch(Dispatchers.IO) {
                val user = User(name, email, photoUrl)
                Repository.insert(user)
                Preferences.activeUser = email

                loggedAccount.postValue(email)
                RxBus.send(RxEvent.LoginStateChanged())

                googleSignInClient?.signOut()
                loadUserProperties(user)
            }
        }
    }

    private fun loadUserProperties(user: User) {
        userName.postValue(user.name)
        userEmail.postValue(user.email)
        userPhotoUrl.postValue(if (user.photoUrl.isNotEmpty()) Uri.parse(user.photoUrl) else defaultPhotoUri)
    }
}