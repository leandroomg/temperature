package co.goshare.temperature.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.goshare.temperature.extensions.postIfNeeded
import co.goshare.temperature.model.TemperatureProvider
import co.goshare.temperature.model.bean.PlaceMonitor
import java.util.*
import kotlin.math.roundToInt

class PlaceItemViewModel(placeMonitor: PlaceMonitor) : ViewModel() {
    val name = MutableLiveData(placeMonitor.place.name)
    val temperature = MutableLiveData("")
    val startDate = MutableLiveData(placeMonitor.startDate)
    val endDate = MutableLiveData(placeMonitor.endDate)
    val observationDate = MutableLiveData<Date?>()
    val isActivated = MutableLiveData(false)
    val showDetails = MutableLiveData(false)
    val loading = MutableLiveData(true)
    val networkError = MutableLiveData(false)

    init {
        TemperatureProvider.fetchTemperature(placeMonitor.place) {
            loading.postIfNeeded(false)
            if (it == null) {
                observationDate.postIfNeeded(null)
                networkError.postIfNeeded(true)
            } else {
                val value = it.temperature.value.roundToInt()
                val units = it.temperature.units
                temperature.postIfNeeded("${value}°${units}")
                observationDate.postIfNeeded(it.observationTime.value)
                networkError.postIfNeeded(false)
            }
        }
    }
}