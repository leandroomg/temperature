package co.goshare.temperature.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.goshare.temperature.model.TemperatureProvider
import co.goshare.temperature.model.bean.PlaceMonitor
import co.goshare.temperature.model.data.Preferences
import co.goshare.temperature.model.data.Repository
import co.goshare.temperature.model.enums.PlaceMonitorFilter
import co.goshare.temperature.model.event.RxBus
import co.goshare.temperature.model.event.RxEvent
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PlacesListViewModel : ViewModel() {
    private val disposables = mutableListOf<Disposable>()
    val placesMonitors = MutableLiveData(listOf<PlaceMonitor>())
    val isRefreshing = MutableLiveData(false)

    var filter = PlaceMonitorFilter.ACTIVATED
        set(value) {
            if (field != value) {
                field = value
                updatePlacesRegister()
            }
        }

    init {
        updatePlacesRegister()
        disposables.add(RxBus.observable(RxEvent.DatabaseUpdated::class.java).subscribe {
            updatePlacesRegister()
        })
        disposables.add(RxBus.observable(RxEvent.LoginStateChanged::class.java).subscribe {
            updatePlacesRegister()
        })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.forEach { it.dispose() }
    }

    fun isUserLogged() = !Preferences.activeUser.isNullOrEmpty()

    fun onRefreshRequested() {
        TemperatureProvider.clearCache()
        updatePlacesRegister()
    }

    fun onDeleteRequested(selectedPositions: Set<Int>) {
        GlobalScope.launch(Dispatchers.IO) {
            val list = selectedPositions.mapNotNull { placesMonitors.value?.get(it) }
            Repository.delete(list)
        }
    }

    private fun updatePlacesRegister() {
        isRefreshing.postValue(true)
        GlobalScope.launch(Dispatchers.IO) {
            val newPlacesRegisters = Repository.getAll(filter).sortedBy { it.place.name }
            placesMonitors.postValue(newPlacesRegisters)
            isRefreshing.postValue(false)
        }
    }
}