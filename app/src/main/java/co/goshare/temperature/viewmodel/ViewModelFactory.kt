package co.goshare.temperature.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.goshare.temperature.util.AndroidResourceProvider

class ViewModelFactory(private val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == UserViewModel::class.java) {
            return UserViewModel(AndroidResourceProvider(context)) as T
        } else {
            throw RuntimeException("Invalid View Model.")
        }
    }
}