package co.goshare.temperature.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.goshare.temperature.model.bean.Place
import co.goshare.temperature.model.bean.PlaceMonitor
import co.goshare.temperature.model.data.Repository
import co.goshare.temperature.util.DateUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*
import android.text.format.DateUtils as AndroidDateUtils
import com.google.android.libraries.places.api.model.Place as AndroidPlace

class AddPlaceViewModel : ViewModel() {
    private var selectedPlace: Place? = null
    private var startCalendar = getDefaultStartCalendar()
    private var endCalendar = getDefaultEndCalendar()
    val startDateText = MutableLiveData(DateUtils.format(startCalendar))
    val endDateText = MutableLiveData(DateUtils.format(endCalendar))
    val taskAddPlaceStatus = MutableLiveData(AddPlaceStatus.NONE)
    val selectPlaceStatus = MutableLiveData(SelectPlaceStatus.NONE)

    fun onPlaceSelected(place: AndroidPlace) {
        Timber.d("Place: ${place.id}, ${place.name}, ${place.latLng}")
        val id = place.id
        val name = place.name
        val latLng = place.latLng
        if (id == null || name == null || latLng == null) {
            selectedPlace = null
            selectPlaceStatus.postValue(SelectPlaceStatus.FAILURE)
        } else {
            selectedPlace = Place(id, name, latLng.latitude, latLng.longitude)
            selectPlaceStatus.postValue(SelectPlaceStatus.SUCCESS)
        }
    }

    fun onDateSelected(dateType: DateType, year: Int, month: Int, day: Int) {
        val calendar = if (dateType == DateType.START_DATE) startCalendar else endCalendar
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)
        val dateText = if (dateType == DateType.START_DATE) startDateText else endDateText
        dateText.postValue(DateUtils.format(calendar))
    }

    fun onTimeSelected(dateType: DateType, hour: Int, minute: Int) {
        val calendar = if (dateType == DateType.START_DATE) startCalendar else endCalendar
        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, minute)
        val dateText = if (dateType == DateType.START_DATE) startDateText else endDateText
        dateText.postValue(DateUtils.format(calendar))

        if (dateType == DateType.START_DATE) {
            endCalendar.timeInMillis = startCalendar.timeInMillis + AndroidDateUtils.DAY_IN_MILLIS
            endDateText.postValue(DateUtils.format(endCalendar))
        }
    }

    fun getMinTimeInMillis(dateType: DateType): Long {
        return when (dateType) {
            DateType.START_DATE -> {
                System.currentTimeMillis() - AndroidDateUtils.MINUTE_IN_MILLIS
            }
            DateType.END_DATE -> {
                startCalendar.timeInMillis + AndroidDateUtils.DAY_IN_MILLIS
            }
        }
    }

    fun getCalendar(dateType: DateType): Calendar {
        return when (dateType) {
            DateType.START_DATE -> startCalendar
            DateType.END_DATE -> endCalendar
        }
    }

    suspend fun addPlace() {
        val place = selectedPlace
        if (place == null) {
            taskAddPlaceStatus.postValue(AddPlaceStatus.INVALID_PLACE_ERROR)
        } else {
            taskAddPlaceStatus.postValue(AddPlaceStatus.RUNNING)
            withContext(Dispatchers.IO) {
                val startDate = Date(startCalendar.timeInMillis)
                val endDate = Date(endCalendar.timeInMillis)
                val placeRegister = PlaceMonitor(0, startDate, endDate, place)
                Repository.insert(placeRegister)
                taskAddPlaceStatus.postValue(AddPlaceStatus.SUCCESS)
            }
        }
    }

    private fun getDefaultStartCalendar(): Calendar {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        return calendar
    }

    private fun getDefaultEndCalendar(): Calendar {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.timeInMillis = calendar.timeInMillis + AndroidDateUtils.DAY_IN_MILLIS
        return calendar
    }

    enum class DateType { START_DATE, END_DATE }

    enum class AddPlaceStatus { NONE, RUNNING, SUCCESS, INVALID_PLACE_ERROR }

    enum class SelectPlaceStatus { NONE, SUCCESS, FAILURE }
}