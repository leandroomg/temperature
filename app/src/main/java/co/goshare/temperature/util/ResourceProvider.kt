package co.goshare.temperature.util

interface ResourceProvider {
    fun getString(stringResId: Int): String
}