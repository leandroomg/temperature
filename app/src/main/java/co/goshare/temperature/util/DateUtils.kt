package co.goshare.temperature.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    private val formatter = SimpleDateFormat("dd/MM/yy HH:mm", Locale.US)
    var timeZone: TimeZone = TimeZone.getDefault()
        set(value) {
            field = value
            formatter.timeZone = timeZone
        }

    @JvmStatic
    fun format(date: Date?): String {
        return if (date == null) "" else formatter.format(date)
    }

    @JvmStatic
    fun format(calendar: Calendar?): String {
        calendar?.timeZone = timeZone
        return if (calendar == null) "" else formatter.format(Date(calendar.timeInMillis))
    }
}