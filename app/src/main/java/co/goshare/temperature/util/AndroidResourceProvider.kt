package co.goshare.temperature.util

import android.content.Context

class AndroidResourceProvider(private val context: Context) : ResourceProvider {
    override fun getString(stringResId: Int): String {
        return context.getString(stringResId)
    }
}