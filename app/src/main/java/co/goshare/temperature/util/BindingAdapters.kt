package co.goshare.temperature.util

import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("bind:imageUri")
    fun setImageUrl(view: ImageView, imageUri: Uri?) {
        Picasso.get()
            .load(imageUri)
            .transform(CircleTransformation())
            .into(view)
    }
}