package co.goshare.temperature.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import timber.log.Timber
import java.lang.ref.WeakReference

object NetworkMonitor {
    lateinit var contextRef: WeakReference<Context>
    private var availableConnections = 0
    private var initialized = false
    private val listeners = mutableSetOf<Listener>()

    @Synchronized
    private fun initialize() {
        initialized = if (initialized) return else true

        val context = requireNotNull(contextRef.get())
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val builder = NetworkRequest.Builder()
        builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        builder.addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)

        val networkRequest = builder.build()
        connectivityManager.registerNetworkCallback(networkRequest, object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                availableConnections++
                if (availableConnections == 1) {
                    listeners.forEach { it.onNetworkAvailable() }
                }
                Timber.d("Connection available. Amount available: $availableConnections")
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                availableConnections--
                if (availableConnections == 0) {
                    listeners.forEach { it.onNetworkLost() }
                }
                Timber.d("Connection lost. Amount available: $availableConnections")
            }
        })
    }

    fun addListener(listener: Listener) {
        listeners.add(listener)
        initialize()
    }

    fun removeListener(listener: Listener) {
        listeners.remove(listener)
    }

    interface Listener {
        fun onNetworkAvailable()

        fun onNetworkLost()
    }
}