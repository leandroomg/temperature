package co.goshare.temperature.extensions

import android.view.Menu
import android.view.MenuItem

fun Menu.getItemById(id: Int, searchInSubMenus: Boolean = true): MenuItem? {
    for (i in 0 until size()) {
        val item = getItem(i)
        if (item.itemId == id) {
            return item
        } else if (searchInSubMenus && item.subMenu != null) {
            val subItem = item.subMenu.getItemById(id, searchInSubMenus)
            if (subItem != null) {
                return subItem
            }
        }
    }
    return null
}