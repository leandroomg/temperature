package co.goshare.temperature.extensions

import android.os.Looper
import androidx.lifecycle.MutableLiveData

fun <T> MutableLiveData<T>.postIfNeeded(newValue: T) {
    if (Looper.myLooper() == Looper.getMainLooper()) {
        value = newValue
    } else {
        postValue(newValue)
    }
}