package co.goshare.temperature.model.enums

enum class PlaceMonitorFilter { ACTIVATED, SCHEDULED, EXPIRED }