package co.goshare.temperature.model.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import co.goshare.temperature.model.bean.PlaceMonitor
import co.goshare.temperature.model.bean.User
import java.lang.ref.WeakReference

@Database(entities = [PlaceMonitor::class, User::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun placeMonitorDao(): PlaceMonitorDao

    abstract fun userDao(): UserDao

    companion object {
        lateinit var contextRef: WeakReference<Context>
        private const val DATABASE_NAME = "AppDatabase"
        val database by lazy { Room.databaseBuilder(contextRef.get()!!, AppDatabase::class.java, DATABASE_NAME).build() }
    }
}