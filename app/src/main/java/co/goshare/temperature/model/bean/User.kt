package co.goshare.temperature.model.bean

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["email"], unique = true)])
data class User(
    val name: String,
    val email: String,
    val photoUrl: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L

    @Ignore
    var placeMonitors: List<PlaceMonitor> = listOf()
}