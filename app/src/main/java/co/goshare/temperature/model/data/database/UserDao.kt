package co.goshare.temperature.model.data.database

import android.database.sqlite.SQLiteConstraintException
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.ABORT
import androidx.room.Query
import co.goshare.temperature.model.bean.User

@Dao
abstract class UserDao {
    private val placeMonitorDao = AppDatabase.database.placeMonitorDao()

    @Query("SELECT * FROM User")
    protected abstract fun defaultGetAll(): List<User>

    @Query("SELECT * FROM User WHERE email = :email")
    protected abstract fun defaultGetByEmail(email: String): User?

    @Insert(onConflict = ABORT)
    protected abstract fun defaultInsert(user: User): Long

    @Delete
    protected abstract fun defaultDelete(user: User)

    @Query("DELETE FROM User")
    protected abstract fun defaultDeleteAll()

    fun getAll(): List<User> {
        val users = defaultGetAll()
        users.forEach { it.placeMonitors = placeMonitorDao.getAllByUserId(it.id) }
        return users
    }

    fun getByEmail(email: String): User? {
        val user = defaultGetByEmail(email)
        user?.let { user.placeMonitors = placeMonitorDao.getAllByUserId(user.id) }
        return user
    }

    fun insert(user: User) {
        try {
            user.id = defaultInsert(user)
            user.placeMonitors.forEach {
                it.ownerId = user.id
                placeMonitorDao.insert(it)
            }
        } catch (e: SQLiteConstraintException) {
            user.id = getByEmail(user.email)?.id ?: 0
            e.printStackTrace()
        }
    }

    fun deleteAll() {
        placeMonitorDao.deleteAll()
        defaultDeleteAll()
    }
}