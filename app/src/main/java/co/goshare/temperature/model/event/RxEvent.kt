package co.goshare.temperature.model.event

class RxEvent {
    class DatabaseUpdated
    class LoginStateChanged
}