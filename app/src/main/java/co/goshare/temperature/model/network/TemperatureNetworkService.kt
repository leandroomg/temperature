package co.goshare.temperature.model.network

import co.goshare.temperature.model.bean.PlaceTemperature
import co.goshare.temperature.util.Constant
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface TemperatureNetworkService {
    @GET("?apikey=${Constant.TEMPERATURE_SERVER_API_KEY}&unit_system=si&fields=temp")
    fun getPlaceTemperatureAsync(@Query("lat") latitude: Double,
                                 @Query("lon") longitude: Double): Deferred<PlaceTemperature>
}