package co.goshare.temperature.model.bean

data class Place(
    val placeId: String,
    val name: String,
    val latitude: Double,
    val longitude: Double
)