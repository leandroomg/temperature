package co.goshare.temperature.model.bean

import com.google.gson.annotations.SerializedName
import java.util.*

data class PlaceTemperature(
    @SerializedName("lat") val latitude: Double,
    @SerializedName("lon") val longitude: Double,
    @SerializedName("temp") val temperature: Temperature,
    @SerializedName("observation_time") val observationTime: ObservationTime
)

data class Temperature(
    val value: Float,
    val units: String
)

data class ObservationTime(
    val value: Date
)