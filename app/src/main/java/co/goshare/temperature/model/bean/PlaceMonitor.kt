package co.goshare.temperature.model.bean

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class PlaceMonitor(
    var ownerId: Long,
    val startDate: Date,
    val endDate: Date,
    @Embedded val place: Place
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L
}