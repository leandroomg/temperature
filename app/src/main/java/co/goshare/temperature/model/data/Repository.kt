package co.goshare.temperature.model.data

import co.goshare.temperature.model.bean.PlaceMonitor
import co.goshare.temperature.model.bean.User
import co.goshare.temperature.model.data.database.AppDatabase
import co.goshare.temperature.model.enums.PlaceMonitorFilter
import co.goshare.temperature.model.enums.PlaceMonitorFilter.*
import co.goshare.temperature.model.event.RxBus
import co.goshare.temperature.model.event.RxEvent
import java.util.*

object Repository {
    private val userDao = AppDatabase.database.userDao()
    private val placeMonitorDao = AppDatabase.database.placeMonitorDao()

    fun getAll(filter: PlaceMonitorFilter): List<PlaceMonitor> {
        val userEmail = Preferences.activeUser ?: return listOf()
        val user = userDao.getByEmail(userEmail) ?: return listOf()

        val registers = placeMonitorDao.getAllByUserId(user.id)
        val currentDate = Date()
        return registers.mapNotNull {
            val isBefore = currentDate.before(it.startDate)
            val isAfter = currentDate.after(it.endDate)
            when {
                filter === ACTIVATED && !isBefore && !isAfter -> it
                filter === SCHEDULED && isBefore -> it
                filter === EXPIRED && isAfter -> it
                else -> null
            }
        }
    }

    fun insert(placeRegister: PlaceMonitor) {
        val userEmail = Preferences.activeUser ?: return
        val user = userDao.getByEmail(userEmail) ?: return

        placeRegister.ownerId = user.id
        placeMonitorDao.insert(placeRegister)
        RxBus.send(RxEvent.DatabaseUpdated())
    }

    fun delete(placeMonitors: List<PlaceMonitor>) {
        placeMonitors.forEach { placeMonitorDao.delete(it) }
        RxBus.send(RxEvent.DatabaseUpdated())
    }

    fun insert(user: User) {
        userDao.insert(user)
    }

    fun getUserById(email: String): User? {
        return userDao.getByEmail(email)
    }
}