package co.goshare.temperature.model.data

import android.content.Context
import java.lang.ref.WeakReference

object Preferences {
    private const val SHARED_PREFS_NAME = "AppPreferences"
    private const val ACTIVE_USER = "activeUser"
    lateinit var contextRef: WeakReference<Context>
    private val preferences by lazy { contextRef.get()!!.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE) }

    @JvmStatic
    var activeUser: String?
        get() = preferences.getString(ACTIVE_USER, null)
        set(value) = preferences
            .edit()
            .putString(ACTIVE_USER, value)
            .apply()
}