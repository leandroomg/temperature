package co.goshare.temperature.model.data.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import co.goshare.temperature.model.bean.PlaceMonitor

@Dao
interface PlaceMonitorDao {
    @Query("SELECT * FROM PlaceMonitor")
    fun getAll(): List<PlaceMonitor>

    @Query("SELECT * FROM PlaceMonitor WHERE ownerId = :userId")
    fun getAllByUserId(userId: Long): List<PlaceMonitor>

    @Insert
    fun insert(placeMonitor: PlaceMonitor)

    @Delete
    fun delete(placeMonitor: PlaceMonitor)

    @Query("DELETE FROM PlaceMonitor")
    fun deleteAll()
}