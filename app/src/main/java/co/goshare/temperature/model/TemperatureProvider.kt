package co.goshare.temperature.model

import co.goshare.temperature.BuildConfig
import co.goshare.temperature.model.bean.ObservationTime
import co.goshare.temperature.model.bean.Place
import co.goshare.temperature.model.bean.PlaceTemperature
import co.goshare.temperature.model.bean.Temperature
import co.goshare.temperature.model.network.NetworkService
import co.goshare.temperature.util.NetworkUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.random.Random

object TemperatureProvider {
    private const val DEBUG_DELAY = 3000L
    private const val DEBUG_MIN_TEMPERATURE = -40.0
    private const val DEBUG_MAX_TEMPERATURE = 40.0
    private const val DEBUG_TEMPERATURE_UNIT = "C"
    private val temperatures = mutableMapOf<String, PlaceTemperature>()

    fun fetchTemperature(place: Place, callback: (temperature: PlaceTemperature?) -> Unit) {
        val localTemperature = temperatures[place.placeId]
        if (localTemperature != null) {
            callback.invoke(localTemperature)
        } else {
            GlobalScope.launch(Dispatchers.IO) {
                try {
                    val latitude = place.latitude
                    val longitude = place.longitude
                    val serverTemperature = if (BuildConfig.DEBUG) {
                        delay(Random.nextLong(DEBUG_DELAY))
                        createFakeTemperature(latitude, longitude)
                    } else {
                        val deferred = NetworkService.temperatureService.getPlaceTemperatureAsync(latitude, longitude)
                        deferred.await()
                    }

                    temperatures[place.placeId] = serverTemperature
                    callback(serverTemperature)
                } catch (e: Exception) {
                    e.printStackTrace()
                    callback(null)
                }
            }
        }
    }

    fun clearCache() {
        temperatures.clear()
    }

    private fun createFakeTemperature(latitude: Double, longitude: Double): PlaceTemperature {
        if (!NetworkUtil.isNetworkAvailable() || Random.nextInt(4) == 1) {
            throw RuntimeException("Simulate a network error.")
        } else {
            val value = Random.nextDouble(DEBUG_MIN_TEMPERATURE, DEBUG_MAX_TEMPERATURE).toFloat()
            val temperature = Temperature(value, DEBUG_TEMPERATURE_UNIT)
            return PlaceTemperature(latitude, longitude, temperature, ObservationTime(Date()))
        }
    }
}