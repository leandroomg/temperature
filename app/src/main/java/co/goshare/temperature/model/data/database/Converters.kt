package co.goshare.temperature.model.data.database

import androidx.room.TypeConverter
import java.util.*

class Converters {
    @TypeConverter
    fun longToDate(value: Long): Date {
        return Date(value)
    }

    @TypeConverter
    fun dateToLong(date: Date): Long {
        return date.time
    }
}