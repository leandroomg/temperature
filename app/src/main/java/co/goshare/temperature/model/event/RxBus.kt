package co.goshare.temperature.model.event

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

object RxBus {
    private val publisher = PublishSubject.create<Any>()

    fun send(event: Any) {
        publisher.onNext(event)
    }

    fun <T> observable(eventType: Class<T>): Observable<T> {
        return publisher.ofType(eventType)
    }
}