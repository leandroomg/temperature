package co.goshare.temperature.ui.fragment

import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.goshare.temperature.R
import co.goshare.temperature.databinding.FragmentPlacesListBinding
import co.goshare.temperature.model.enums.PlaceMonitorFilter
import co.goshare.temperature.ui.adapter.PlacesAdapter
import co.goshare.temperature.util.NetworkMonitor
import co.goshare.temperature.util.NetworkUtil
import co.goshare.temperature.viewmodel.PlacesListViewModel
import com.google.android.material.snackbar.Snackbar

class PlacesListFragment : Fragment(), NetworkMonitor.Listener {
    private lateinit var placesAdapter: PlacesAdapter
    private lateinit var binding: FragmentPlacesListBinding
    private val viewModel: PlacesListViewModel by viewModels()
    private var noNetConnectivitySnackBar: Snackbar? = null
    private var hasPlaceSelection = false

    private val backPressedCallback = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            placesAdapter.clearSelection()
        }
    }

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, backPressedCallback)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel.filter = PlaceMonitorFilter.ACTIVATED
        binding = FragmentPlacesListBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setEvents()
        setLayoutProperties()
        NetworkMonitor.addListener(this)
        if (!NetworkUtil.isNetworkAvailable()) {
            onNetworkLost()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        NetworkMonitor.removeListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.places_list_menu, menu)
        if (hasPlaceSelection) {
            menu.findItem(R.id.menu_filter).isVisible = false
            menu.findItem(R.id.menu_refresh).isVisible = false
        } else {
            menu.findItem(R.id.menu_delete).isVisible = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_refresh -> {
                binding.swipeRefreshLayout.isRefreshing = true
                binding.swipeRefreshLayout.postDelayed({ viewModel.onRefreshRequested() }, DELAY_TO_SHOW_REFRESH)
                true
            }
            R.id.menu_filter -> {
                showFilteringPopUpMenu()
                true
            }
            R.id.menu_delete -> {
                showDeleteConfirmationDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNetworkAvailable() {
        noNetConnectivitySnackBar?.dismiss()
        noNetConnectivitySnackBar = null
    }

    override fun onNetworkLost() {
        if (noNetConnectivitySnackBar?.isShown != true) {
            noNetConnectivitySnackBar =
                Snackbar.make(binding.addButton, R.string.no_net_connectivity, Snackbar.LENGTH_INDEFINITE)
            noNetConnectivitySnackBar?.show()
        }
    }

    private fun setEvents() {
        binding.addButton.setOnClickListener { onAddButtonClicked() }
    }

    private fun setLayoutProperties() {
        placesAdapter = PlacesAdapter(viewModel, viewLifecycleOwner)
        placesAdapter.listener = object : PlacesAdapter.Listener {
            override fun onItemStateChanged() {
                onListSelectionChanged()
            }
        }

        val layoutManager = LinearLayoutManager(requireContext())
        val dividerItemDecoration = DividerItemDecoration(context, layoutManager.orientation)

        binding.placesListView.layoutManager = layoutManager
        binding.placesListView.adapter = placesAdapter
        binding.placesListView.addItemDecoration(dividerItemDecoration)
    }

    private fun onListSelectionChanged() {
        val newHasPlaceSelection = placesAdapter.hasSelection()
        if (newHasPlaceSelection != hasPlaceSelection) {
            hasPlaceSelection = newHasPlaceSelection
            backPressedCallback.isEnabled = hasPlaceSelection
            activity?.invalidateOptionsMenu()
        }
    }

    private fun onAddButtonClicked() {
        binding.addButton.isEnabled = false
        if (viewModel.isUserLogged()) {
            findNavController().navigate(R.id.action_add_place)
        } else {
            showRequestLoginDialog()
        }
    }

    private fun showDeleteConfirmationDialog() {
        val alertDialog = AlertDialog.Builder(requireContext()).create()
        alertDialog.setTitle(getString(R.string.dialog_delete_confirmation_title))
        alertDialog.setMessage(getString(R.string.dialog_delete_confirmation_message))
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.dialog_btn_no)) { _, _ -> }
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_btn_yes)) { _, _ ->
            viewModel.onDeleteRequested(placesAdapter.selection)
            placesAdapter.clearSelection()
        }
        alertDialog.show()
    }

    private fun showRequestLoginDialog() {
        val alertDialog = AlertDialog.Builder(requireContext()).create()
        alertDialog.setTitle(getString(R.string.dialog_request_login_title))
        alertDialog.setMessage(getString(R.string.dialog_request_login_message))
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.dialog_btn_no)) { _, _ -> }
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_btn_yes)) { _, _ ->
            activity?.findViewById<DrawerLayout>(R.id.drawer_layout)?.open()
        }
        alertDialog.setOnDismissListener {
            binding.addButton.isEnabled = true
        }
        alertDialog.show()
    }

    private fun showFilteringPopUpMenu() {
        val view = activity?.findViewById<View>(R.id.menu_filter) ?: return
        PopupMenu(requireContext(), view).run {
            menuInflater.inflate(R.menu.places_list_filter_menu, menu)

            setOnMenuItemClickListener {
                viewModel.filter = when (it.itemId) {
                    R.id.menu_filter_activated -> PlaceMonitorFilter.ACTIVATED
                    R.id.menu_filter_scheduled -> PlaceMonitorFilter.SCHEDULED
                    else -> PlaceMonitorFilter.EXPIRED
                }
                view.isActivated = viewModel.filter !== PlaceMonitorFilter.ACTIVATED
                val emptyListSubtitle = binding.root.findViewById<TextView>(R.id.emptyListSubtitle)
                emptyListSubtitle.setText(when (viewModel.filter) {
                    PlaceMonitorFilter.ACTIVATED -> R.string.empty_list_activated_subtitle
                    PlaceMonitorFilter.SCHEDULED -> R.string.empty_list_scheduled_subtitle
                    PlaceMonitorFilter.EXPIRED -> R.string.empty_list_expired_subtitle
                })
                true
            }
            show()
        }
    }

    companion object {
        private const val DELAY_TO_SHOW_REFRESH = 1000L
    }
}