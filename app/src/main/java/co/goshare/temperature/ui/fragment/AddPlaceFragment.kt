package co.goshare.temperature.ui.fragment

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.goshare.temperature.R
import co.goshare.temperature.databinding.FragmentAddPlaceBinding
import co.goshare.temperature.viewmodel.AddPlaceViewModel
import co.goshare.temperature.viewmodel.AddPlaceViewModel.DateType
import co.goshare.temperature.viewmodel.AddPlaceViewModel.SelectPlaceStatus
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.annotations.NotNull
import timber.log.Timber
import java.util.*

class AddPlaceFragment : Fragment() {
    private lateinit var binding: FragmentAddPlaceBinding
    private val viewModel: AddPlaceViewModel by viewModels()
    private var showProgressStartTime = 0L

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Places.initialize(context, getString(R.string.places_api_key))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentAddPlaceBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setEvents()
        setLayoutProperties()
    }

    private fun setEvents() {
        binding.startDateTextView.setOnClickListener {
            showDatePicker(DateType.START_DATE)
        }
        binding.startDateImageView.setOnClickListener {
            showDatePicker(DateType.START_DATE)
        }
        binding.endDateTextView.setOnClickListener {
            showDatePicker(DateType.END_DATE)
        }
        binding.endDateImageView.setOnClickListener {
            showDatePicker(DateType.END_DATE)
        }
        binding.doneButton.setOnClickListener {
            onDoneButtonClicked()
        }
        viewModel.taskAddPlaceStatus.observe(viewLifecycleOwner, Observer {
            onAddPlaceStatusChanged(it)
        })
        viewModel.selectPlaceStatus.observe(viewLifecycleOwner, Observer {
            onSelectPlaceStatusChanged(it)
        })
    }

    private fun setLayoutProperties() {
        val autocompleteFragment =
            childFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG))
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(@NotNull place: Place) {
                viewModel.onPlaceSelected(place)
            }

            override fun onError(status: Status) {
                Timber.d("An error occurred: $status")
            }
        })
    }

    private fun showDatePicker(dateType: DateType) {
        val calendar = viewModel.getCalendar(dateType)
        val datePickerDialog = DatePickerDialog(
            requireContext(),
            OnDateSetListener { _, year, month, day ->
                viewModel.onDateSelected(dateType, year, month, day)
                showTimePicker(dateType)
            },
            calendar[Calendar.YEAR],
            calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        datePickerDialog.datePicker.minDate = viewModel.getMinTimeInMillis(dateType)
        datePickerDialog.show()
    }

    private fun showTimePicker(dateType: DateType) {
        val calendar = viewModel.getCalendar(dateType)
        val timePickerDialog = TimePickerDialog(
            requireContext(),
            OnTimeSetListener { _, hour, minute ->
                viewModel.onTimeSelected(dateType, hour, minute)
            },
            calendar[Calendar.HOUR_OF_DAY],
            calendar[Calendar.MINUTE],
            true
        )
        timePickerDialog.show()
    }

    private fun onDoneButtonClicked() {
        binding.doneButton.isEnabled = false
        GlobalScope.launch { viewModel.addPlace() }
    }

    private fun onAddPlaceStatusChanged(addPlaceStatus: AddPlaceViewModel.AddPlaceStatus) {
        when (addPlaceStatus) {
            AddPlaceViewModel.AddPlaceStatus.NONE -> hideProgress()
            AddPlaceViewModel.AddPlaceStatus.RUNNING -> showProgress()
            AddPlaceViewModel.AddPlaceStatus.SUCCESS -> {
                hideProgress {
                    findNavController().popBackStack()
                }
            }
            AddPlaceViewModel.AddPlaceStatus.INVALID_PLACE_ERROR -> {
                binding.doneButton.isEnabled = true
                hideProgress {
                    Snackbar.make(binding.doneButton, R.string.add_place_invalid_name_error, Snackbar.LENGTH_LONG)
                        .show()
                }
            }
        }
    }

    private fun onSelectPlaceStatusChanged(selectPlaceStatus: SelectPlaceStatus?) {
        if (selectPlaceStatus == SelectPlaceStatus.FAILURE) {
            Snackbar.make(binding.doneButton, R.string.add_place_select_place_error, Snackbar.LENGTH_LONG).show()
            val autocompleteFragment =
                childFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
            autocompleteFragment.setText("")
        }
    }

    private fun showProgress() {
        showProgressStartTime = System.currentTimeMillis()
        binding.progressContainer.visibility = View.VISIBLE
    }

    private fun hideProgress(callback: (() -> Unit)? = null) {
        val hideDelay = MIN_TIME_SHOWING_PROGRESS - (System.currentTimeMillis() - showProgressStartTime)
        binding.progressContainer.postDelayed({
            binding.progressContainer.visibility = View.GONE
            callback?.invoke()
        }, hideDelay)
    }

    companion object {
        private const val MIN_TIME_SHOWING_PROGRESS = 1250L
    }
}