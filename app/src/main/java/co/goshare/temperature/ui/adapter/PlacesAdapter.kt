package co.goshare.temperature.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import co.goshare.temperature.R
import co.goshare.temperature.databinding.ListItemPlaceBinding
import co.goshare.temperature.model.bean.PlaceMonitor
import co.goshare.temperature.ui.adapter.PlacesAdapter.PlaceViewHolder
import co.goshare.temperature.viewmodel.PlaceItemViewModel
import co.goshare.temperature.viewmodel.PlacesListViewModel

class PlacesAdapter(
    private val viewModel: PlacesListViewModel,
    private val viewLifecycleOwner: LifecycleOwner
) : RecyclerView.Adapter<PlaceViewHolder>() {

    private var placeMonitors = viewModel.placesMonitors.value ?: listOf()
    private val placeMonitorsViewModelMap = mutableMapOf<Int, PlaceItemViewModel>()
    private val placesMonitorsObserver = PlaceMonitorObserver()
    val selection = mutableSetOf<Int>()
    var listener: Listener? = null

    init {
        setHasStableIds(true)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        viewModel.placesMonitors.observeForever(placesMonitorsObserver)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        viewModel.placesMonitors.removeObserver(placesMonitorsObserver)
    }

    override fun getItemViewType(position: Int) = R.layout.list_item_place

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemCount() = placeMonitors.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ListItemPlaceBinding>(layoutInflater, viewType, parent, false)
        return PlaceViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PlaceViewHolder, position: Int) {
        val viewModel = getViewModel(position)
        holder.bind(viewModel)
        holder.itemView.setOnClickListener {
            if (hasSelection()) {
                switchSelectionState(viewModel, position)
            } else {
                viewModel.showDetails.value = viewModel.showDetails.value != true
            }
        }
        holder.itemView.setOnLongClickListener {
            switchSelectionState(viewModel, position)
            true
        }
    }

    fun hasSelection() = selection.size > 0

    fun clearSelection() {
        placeMonitorsViewModelMap.values.forEach {
            if (it.isActivated.value == true) {
                it.isActivated.value = false
            }
        }
        selection.clear()
        listener?.onItemStateChanged()
    }

    private fun getViewModel(position: Int): PlaceItemViewModel {
        var viewModel = placeMonitorsViewModelMap[position]
        if (viewModel == null) {
            viewModel = PlaceItemViewModel(placeMonitors[position])
            placeMonitorsViewModelMap[position] = viewModel
        }
        return viewModel
    }

    private fun switchSelectionState(viewModel: PlaceItemViewModel, position: Int) {
        if (viewModel.isActivated.value == true) {
            viewModel.isActivated.value = false
            selection.remove(position)
        } else {
            viewModel.isActivated.value = true
            selection.add(position)
        }
        listener?.onItemStateChanged()
    }

    interface Listener {
        fun onItemStateChanged()
    }

    inner class PlaceViewHolder(private val binding: ListItemPlaceBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel: PlaceItemViewModel) {
            binding.viewModel = viewModel
            binding.lifecycleOwner = viewLifecycleOwner
        }
    }

    inner class PlaceMonitorObserver: Observer<List<PlaceMonitor>> {
        override fun onChanged(t: List<PlaceMonitor>?) {
            placeMonitors = viewModel.placesMonitors.value ?: listOf()
            placeMonitorsViewModelMap.clear()
            notifyDataSetChanged()
        }
    }
}