package co.goshare.temperature.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import co.goshare.temperature.R
import co.goshare.temperature.databinding.LayoutNavHeaderBinding
import co.goshare.temperature.extensions.getItemById
import co.goshare.temperature.viewmodel.UserViewModel
import co.goshare.temperature.viewmodel.ViewModelFactory
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView: NavigationView
    private lateinit var navController: NavController
    private val userViewModel: UserViewModel by viewModels { ViewModelFactory(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setReferences()
        setLayoutProperties()
        setUserViewModel()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            handleSignInResult(data)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.nav_add_account,
            R.id.nav_change_account -> {
                userViewModel.login()
                true
            }
            R.id.nav_exit_account -> {
                GlobalScope.launch { userViewModel.logout() }
                true
            }
            else -> {
                val handled = NavigationUI.onNavDestinationSelected(item, navController)
                if (handled) {
                    drawerLayout.close()
                }
                handled
            }
        }
    }

    private fun setReferences() {
        navView = findViewById(R.id.nav_view)
        drawerLayout = findViewById(R.id.drawer_layout)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.nav_home), drawerLayout)
        navController = findNavController(R.id.nav_host_fragment)
    }

    private fun setLayoutProperties() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val navHeaderBinding = LayoutNavHeaderBinding.inflate(LayoutInflater.from(this))
        navHeaderBinding.viewModel = userViewModel
        navHeaderBinding.lifecycleOwner = this

        navController.addOnDestinationChangedListener { _, destination, _ ->
            navView.menu.getItemById(destination.id)?.isChecked = true
        }
        setupActionBarWithNavController(navController, appBarConfiguration)

        navView.addHeaderView(navHeaderBinding.root)
        navView.setNavigationItemSelectedListener(this)
        setNavigationViewMenuItems()
    }

    private fun setNavigationViewMenuItems() {
        val isLogged = userViewModel.isLogged()
        navView.menu.getItemById(R.id.nav_add_account)?.isVisible = !isLogged
        navView.menu.getItemById(R.id.nav_change_account)?.isVisible = isLogged
        navView.menu.getItemById(R.id.nav_exit_account)?.isVisible = isLogged
        navView.menu.getItemById(R.id.nav_home)?.isChecked = true
    }

    private fun setUserViewModel() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        val googleSignInClient = GoogleSignIn.getClient(this, gso)

        userViewModel.googleSignInClient = googleSignInClient
        userViewModel.signInRequested.observe(this, Observer { value ->
            if (value) {
                val signInIntent: Intent = googleSignInClient.signInIntent
                startActivityForResult(signInIntent, RC_SIGN_IN)
                userViewModel.onSignInFlowStarted()
            }
        })
        userViewModel.loggedAccount.observe(this, Observer {
            setNavigationViewMenuItems()
        })
    }

    private fun handleSignInResult(data: Intent?) {
        try {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            userViewModel.onSignInResultReceived(task)
        } catch (e: ApiException) {
            Snackbar.make(findViewById(R.id.addButton), R.string.login_error, Snackbar.LENGTH_LONG).show()
            drawerLayout.close()
        }
    }

    companion object {
        private const val RC_SIGN_IN = 1000
    }
}