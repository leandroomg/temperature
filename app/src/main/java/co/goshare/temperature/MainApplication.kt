package co.goshare.temperature

import androidx.multidex.MultiDexApplication
import co.goshare.temperature.model.data.Preferences
import co.goshare.temperature.model.data.database.AppDatabase
import co.goshare.temperature.util.NetworkMonitor
import co.goshare.temperature.util.NetworkUtil
import timber.log.Timber
import java.lang.ref.WeakReference

class MainApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        configureTimber()
        AppDatabase.contextRef = WeakReference(applicationContext)
        Preferences.contextRef = WeakReference(applicationContext)
        NetworkMonitor.contextRef = WeakReference(applicationContext)
        NetworkUtil.contextRef = WeakReference(applicationContext)
    }

    private fun configureTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}