<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <import type="android.view.ViewGroup.LayoutParams" />

        <import type="co.goshare.temperature.util.DateUtils" />

        <variable
            name="viewModel"
            type="co.goshare.temperature.viewmodel.PlaceItemViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@drawable/place_item_background"
        android:padding="@dimen/space_10"
        app:activated="@{viewModel.isActivated}">

        <TextView
            android:id="@+id/temperatureTextView"
            android:layout_width="@dimen/place_item_temperature_width"
            android:layout_height="wrap_content"
            android:clickable="false"
            android:gravity="center"
            android:text="@{viewModel.temperature}"
            android:textColor="@color/black"
            android:textSize="@dimen/place_item_name_text_size"
            android:visibility="@{(viewModel.loading || viewModel.networkError) ? View.INVISIBLE : View.VISIBLE}"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            tools:text="10ºC" />

        <ProgressBar
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:indeterminate="true"
            android:visibility="@{viewModel.loading ? View.VISIBLE : View.GONE}"
            app:layout_constraintBottom_toBottomOf="@id/temperatureTextView"
            app:layout_constraintDimensionRatio="W,1:1"
            app:layout_constraintHorizontal_bias="0.3"
            app:layout_constraintLeft_toLeftOf="@id/temperatureTextView"
            app:layout_constraintRight_toRightOf="@id/temperatureTextView"
            app:layout_constraintTop_toTopOf="@id/temperatureTextView" />

        <ImageView
            android:layout_width="wrap_content"
            android:layout_height="0dp"
            android:adjustViewBounds="true"
            android:paddingTop="@dimen/space_4"
            android:paddingBottom="@dimen/space_4"
            android:scaleType="fitCenter"
            android:src="@drawable/place_item_network_fail"
            android:visibility="@{viewModel.networkError ? View.VISIBLE : View.GONE}"
            app:layout_constraintBottom_toBottomOf="@id/temperatureTextView"
            app:layout_constraintHorizontal_bias="0.3"
            app:layout_constraintLeft_toLeftOf="@id/temperatureTextView"
            app:layout_constraintRight_toRightOf="@id/temperatureTextView"
            app:layout_constraintTop_toTopOf="@id/temperatureTextView" />

        <TextView
            android:id="@+id/placeTextView"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:clickable="false"
            android:text="@{viewModel.name}"
            android:textColor="@color/black"
            android:textSize="@dimen/place_item_name_text_size"
            app:layout_constraintLeft_toRightOf="@id/temperatureTextView"
            app:layout_constraintRight_toRightOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            tools:text="Place" />

        <TextView
            android:id="@+id/observationDateTitleTextView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:clickable="false"
            android:text="@string/place_item_observation_date"
            android:textColor="@color/black"
            android:textSize="@dimen/place_item_date_text_size"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintTop_toBottomOf="@id/placeTextView" />

        <TextView
            android:id="@+id/observationDateTextView"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="@dimen/space_5"
            android:clickable="false"
            android:text="@{DateUtils.format(viewModel.observationDate)}"
            android:textColor="@color/black"
            android:textSize="@dimen/place_item_date_text_size"
            app:layout_constraintLeft_toRightOf="@+id/observationDateTitleTextView"
            app:layout_constraintRight_toRightOf="parent"
            app:layout_constraintTop_toBottomOf="@id/placeTextView"
            tools:text="15/08/20 18:00" />

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:clickable="false"
            android:visibility="@{viewModel.showDetails ? View.VISIBLE : View.GONE}"
            app:layout_constraintTop_toBottomOf="@+id/observationDateTitleTextView">

            <TextView
                android:id="@+id/startDateTitleTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/place_item_start_date"
                android:textColor="@color/black"
                android:textSize="@dimen/place_item_date_text_size"
                app:layout_constraintLeft_toLeftOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                tools:text="Start Date" />

            <TextView
                android:id="@+id/startDateTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/space_5"
                android:text="@{DateUtils.format(viewModel.startDate)}"
                android:textColor="@color/black"
                android:textSize="@dimen/place_item_date_text_size"
                app:layout_constraintLeft_toRightOf="@id/startDateTitleTextView"
                app:layout_constraintTop_toTopOf="parent"
                tools:text="15/08/20 18:00" />

            <TextView
                android:id="@+id/endDateTitleTextView"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:text="@string/place_item_end_date"
                android:textColor="@color/black"
                android:textSize="@dimen/place_item_date_text_size"
                app:layout_constraintLeft_toLeftOf="@id/startDateTitleTextView"
                app:layout_constraintRight_toRightOf="@id/startDateTitleTextView"
                app:layout_constraintTop_toBottomOf="@id/startDateTitleTextView"
                tools:text="End Date" />

            <TextView
                android:id="@+id/endDateTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/space_5"
                android:text="@{DateUtils.format(viewModel.endDate)}"
                android:textColor="@color/black"
                android:textSize="@dimen/place_item_date_text_size"
                app:layout_constraintLeft_toRightOf="@+id/endDateTitleTextView"
                app:layout_constraintTop_toBottomOf="@id/startDateTitleTextView"
                tools:text="15/08/20 18:00" />
        </androidx.constraintlayout.widget.ConstraintLayout>
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>