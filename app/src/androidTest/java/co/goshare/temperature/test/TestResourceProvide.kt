package co.goshare.temperature.test

import co.goshare.temperature.R
import co.goshare.temperature.util.ResourceProvider

class TestResourceProvide : ResourceProvider {
    override fun getString(stringResId: Int): String {
        return if (stringResId == R.string.nav_header_not_logged) {
            NAV_HEADER_NOT_LOGGED
        } else {
            ""
        }
    }

    companion object {
        const val NAV_HEADER_NOT_LOGGED = "nav_header_not_logged"
    }
}