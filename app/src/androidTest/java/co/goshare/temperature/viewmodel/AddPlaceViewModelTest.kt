package co.goshare.temperature.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.goshare.temperature.util.DateUtils
import co.goshare.temperature.viewmodel.AddPlaceViewModel.AddPlaceStatus
import co.goshare.temperature.viewmodel.AddPlaceViewModel.DateType.END_DATE
import co.goshare.temperature.viewmodel.AddPlaceViewModel.DateType.START_DATE
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.Place
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.util.*

@RunWith(AndroidJUnit4::class)
class AddPlaceViewModelTest {
    private var viewModel = AddPlaceViewModel()

    @Before
    fun before() {
        viewModel = AddPlaceViewModel()
        DateUtils.timeZone = utcTimezone
    }

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun onPlaceSelected() {
        val place : Place = mock(Place::class.java)
        `when`(place.id).thenReturn("id1")
        `when`(place.name).thenReturn("Fortaleza")
        `when`(place.latLng).thenReturn(LatLng(1.0, 1.0))
        viewModel.onPlaceSelected(place)
        assertTrue(viewModel.selectPlaceStatus.value === AddPlaceViewModel.SelectPlaceStatus.SUCCESS)
    }

    @Test
    fun onPlaceSelected_invalidPlace() {
        val place : Place = mock(Place::class.java)

        // Id null
        `when`(place.id).thenReturn(null)
        `when`(place.name).thenReturn("name")
        `when`(place.latLng).thenReturn(LatLng(1.0, 1.0))
        viewModel.onPlaceSelected(place)
        assertTrue(viewModel.selectPlaceStatus.value === AddPlaceViewModel.SelectPlaceStatus.FAILURE)

        // Name null
        `when`(place.id).thenReturn("id1")
        `when`(place.name).thenReturn(null)
        viewModel.onPlaceSelected(place)
        assertTrue(viewModel.selectPlaceStatus.value === AddPlaceViewModel.SelectPlaceStatus.FAILURE)

        // LatLgn null
        `when`(place.name).thenReturn("Fortaleza")
        `when`(place.latLng).thenReturn(null)
        viewModel.onPlaceSelected(place)
        assertTrue(viewModel.selectPlaceStatus.value === AddPlaceViewModel.SelectPlaceStatus.FAILURE)
    }

    @Test
    fun onDateSelected() {
        viewModel.onDateSelected(START_DATE, 2021, 5, 15)
        assertTrue(viewModel.startDateText.value?.startsWith("15/06/21") == true)

        viewModel.onDateSelected(END_DATE, 2021, 5, 15)
        assertTrue(viewModel.endDateText.value?.startsWith("15/06/21") == true)
    }

    @Test
    fun onTimeSelected() {
        viewModel.onTimeSelected(START_DATE, 11, 15)
        assertTrue(viewModel.startDateText.value?.endsWith("11:15") == true)

        viewModel.onTimeSelected(END_DATE, 11, 15)
        assertTrue(viewModel.endDateText.value?.endsWith("11:15") == true)
    }

    @Test
    fun addPlace() {
        runBlocking {
            assertEquals(AddPlaceStatus.NONE, viewModel.taskAddPlaceStatus.value)

            viewModel.addPlace()
            assertEquals(AddPlaceStatus.INVALID_PLACE_ERROR, viewModel.taskAddPlaceStatus.value)

            val place : Place = mock(Place::class.java)
            `when`(place.id).thenReturn("id1")
            `when`(place.name).thenReturn("Fortaleza")
            `when`(place.latLng).thenReturn(LatLng(1.0, 1.0))
            viewModel.onPlaceSelected(place)

            viewModel.addPlace()
            assertEquals(AddPlaceStatus.SUCCESS, viewModel.taskAddPlaceStatus.value)
        }
    }

    companion object {
        private val utcTimezone = TimeZone.getTimeZone("UTC")
    }
}