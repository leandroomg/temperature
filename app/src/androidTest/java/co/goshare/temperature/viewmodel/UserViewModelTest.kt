package co.goshare.temperature.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.goshare.temperature.model.data.Preferences
import co.goshare.temperature.test.TestResourceProvide
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserViewModelTest {
    private val viewModel = UserViewModel(TestResourceProvide())

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun isLogged() {
        Preferences.activeUser = null
        assertFalse(viewModel.isLogged())

        Preferences.activeUser = EMAIL
        assertTrue(viewModel.isLogged())

        Preferences.activeUser = ""
        assertFalse(viewModel.isLogged())
    }

    @Test
    fun logout() {
        viewModel.userName.postValue(NAME)
        viewModel.userEmail.postValue(EMAIL)
        Preferences.activeUser = EMAIL

        runBlocking {
            viewModel.logout()
            assertFalse(viewModel.isLogged())
            assertEquals(TestResourceProvide.NAV_HEADER_NOT_LOGGED, viewModel.userName.value)
            assertEquals("", viewModel.userEmail.value)
        }
    }

    companion object {
        private const val NAME = "Test"
        private const val EMAIL = "test@gmail.com"
    }
}