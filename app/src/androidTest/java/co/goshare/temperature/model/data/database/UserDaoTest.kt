package co.goshare.temperature.model.data.database

import androidx.test.ext.junit.runners.AndroidJUnit4
import co.goshare.temperature.model.bean.Place
import co.goshare.temperature.model.bean.PlaceMonitor
import co.goshare.temperature.model.bean.User
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class UserDaoTest {
    private lateinit var userDao: UserDao
    private lateinit var placeMonitorDao: PlaceMonitorDao

    @Before
    fun before() {
        userDao = AppDatabase.database.userDao()
        userDao.deleteAll()
        placeMonitorDao = AppDatabase.database.placeMonitorDao()

        // Reset the user id because the insert update this value in the object received
        USER_1.id = 0
        USER_2.id = 0
    }

    @After
    fun after() {
        userDao.deleteAll()
    }

    @Test
    fun getAll() {
        userDao.insert(USER_1)
        userDao.insert(USER_2)
        val users = userDao.getAll()
        assertEquals(2, users.size)
        assertEquals(USER_2, users[1])
    }

    @Test
    fun getByEmail() {
        userDao.insert(USER_1)
        userDao.insert(USER_2)
        val user = userDao.getByEmail(USER_1.email)
        assertEquals(USER_1, user)
        assertEquals(USER_1.placeMonitors.size, user?.placeMonitors?.size)
        assertEquals(USER_1.placeMonitors[0].place, user?.placeMonitors?.get(0)?.place)
    }

    @Test
    fun insert() {
        userDao.insert(USER_1)
        userDao.insert(USER_2)

        // Check if the insert update the object with the auto generated id
        assertNotEquals(0, USER_1.id)
        assertNotEquals(0, USER_2.id)

        val users = userDao.getAll()
        assertEquals(2, users.size)
        assertEquals(USER_1, users[0])
        assertEquals(USER_2, users[1])

        // Check if the place monitors are assigned
        assertEquals(USER_1.placeMonitors.size, users[0].placeMonitors.size)
        assertEquals(USER_1.placeMonitors[0].place, users[0].placeMonitors[0].place)
        assertEquals(USER_2.placeMonitors.size, users[1].placeMonitors.size)
        assertEquals(USER_2.placeMonitors[0].place, users[1].placeMonitors[0].place)
    }

    @Test
    fun insert_duplicatedUserEmail() {
        userDao.insert(USER_1)
        userDao.insert(USER_1)

        // Check if the insert was aborted after the email conflict
        assertEquals(1, userDao.getAll().size)

        // Check if the placeMonitors were not inserted after the email conflict
        assertEquals(2, placeMonitorDao.getAll().size)
    }

    companion object {
        private val USER_1 = User("name1", "email1", "photo1")
        private val USER_2 = User("name2", "email2", "photo2")
        private val PLACE_MONITOR_1 = PlaceMonitor(0, Date(), Date(), Place("id1", "Fortaleza", 3.0, 34.0))
        private val PLACE_MONITOR_2 = PlaceMonitor(0, Date(), Date(), Place("id2", "Recife", 10.0, 34.0))
        private val PLACE_MONITOR_3 = PlaceMonitor(0, Date(), Date(), Place("id3", "Rio", 12.0, 35.0))
        private val PLACE_MONITOR_4 = PlaceMonitor(0, Date(), Date(), Place("id1", "Fortaleza", 14.0, 37.0))
        private val PLACE_MONITOR_5 = PlaceMonitor(0, Date(), Date(), Place("id4", "São Paulo", 15.0, 37.0))

        init {
            USER_1.placeMonitors = listOf(PLACE_MONITOR_1, PLACE_MONITOR_2)
            USER_2.placeMonitors = listOf(PLACE_MONITOR_3, PLACE_MONITOR_4, PLACE_MONITOR_5)
        }
    }
}