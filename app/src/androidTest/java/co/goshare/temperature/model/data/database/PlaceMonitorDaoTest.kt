package co.goshare.temperature.model.data.database

import androidx.test.ext.junit.runners.AndroidJUnit4
import co.goshare.temperature.model.bean.Place
import co.goshare.temperature.model.bean.PlaceMonitor
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class PlaceMonitorDaoTest {
    private lateinit var placeMonitorDao: PlaceMonitorDao

    @Before
    fun before() {
        placeMonitorDao = AppDatabase.database.placeMonitorDao()
        placeMonitorDao.deleteAll()
    }

    @After
    fun after() {
        placeMonitorDao.deleteAll()
    }

    @Test
    fun getAll() {
        placeMonitorDao.insert(PLACE_MONITOR_1)
        placeMonitorDao.insert(PLACE_MONITOR_2)
        placeMonitorDao.insert(PLACE_MONITOR_3)
        placeMonitorDao.insert(PLACE_MONITOR_4)
        val registers = placeMonitorDao.getAll()
        assertEquals(4, registers.size)
        assertEquals(PLACE_MONITOR_3, registers[2])
    }

    @Test
    fun getAllByUser() {
        placeMonitorDao.insert(PLACE_MONITOR_1)
        placeMonitorDao.insert(PLACE_MONITOR_2)
        placeMonitorDao.insert(PLACE_MONITOR_3)
        placeMonitorDao.insert(PLACE_MONITOR_4)
        val registers = placeMonitorDao.getAllByUserId(USER_ID_2)
        assertEquals(2, registers.size)
        assertEquals(PLACE_MONITOR_3, registers[0])
    }

    @Test
    fun insert() {
        placeMonitorDao.insert(PLACE_MONITOR_1)
        val registers = placeMonitorDao.getAll()
        assertEquals(1, registers.size)
        assertEquals(PLACE_MONITOR_1, registers[0])

        placeMonitorDao.insert(PLACE_MONITOR_2)
        val registers2 = placeMonitorDao.getAll()
        assertEquals(2, registers2.size)
        assertEquals(PLACE_MONITOR_1, registers2[0])
        assertEquals(PLACE_MONITOR_2, registers2[1])
    }

    @Test
    fun insert_duplicatedRegister() {
        placeMonitorDao.insert(PLACE_MONITOR_1)
        placeMonitorDao.insert(PLACE_MONITOR_1)
        assertEquals(2, placeMonitorDao.getAll().size)
    }

    @Test
    fun insert_byUser() {
        placeMonitorDao.insert(PLACE_MONITOR_1)
        placeMonitorDao.insert(PLACE_MONITOR_2)
        placeMonitorDao.insert(PLACE_MONITOR_3)
        placeMonitorDao.insert(PLACE_MONITOR_4)

        val placeMonitors = placeMonitorDao.getAllByUserId(USER_ID_2)
        assertEquals(2, placeMonitors.size)
        assertEquals(PLACE_MONITOR_3, placeMonitors[0])
        assertEquals(PLACE_MONITOR_4, placeMonitors[1])
    }

    @Test
    fun delete() {
        placeMonitorDao.insert(PLACE_MONITOR_1)
        placeMonitorDao.insert(PLACE_MONITOR_2)
        placeMonitorDao.delete(placeMonitorDao.getAll()[0])
        assertEquals(1, placeMonitorDao.getAll().size)
        assertEquals(PLACE_MONITOR_2.place, placeMonitorDao.getAll()[0].place)
    }

    @Test
    fun deleteAll() {
        placeMonitorDao.insert(PLACE_MONITOR_1)
        placeMonitorDao.insert(PLACE_MONITOR_2)
        placeMonitorDao.deleteAll()
        assertEquals(0, placeMonitorDao.getAll().size)
    }

    companion object {
        private const val USER_ID_1 = 1L
        private const val USER_ID_2 = 2L
        private val PLACE_MONITOR_1 = PlaceMonitor(USER_ID_1, Date(), Date(), Place("id1", "Fortaleza", 3.0, 34.0))
        private val PLACE_MONITOR_2 = PlaceMonitor(USER_ID_1, Date(), Date(), Place("id2", "Recife", 10.0, 34.0))
        private val PLACE_MONITOR_3 = PlaceMonitor(USER_ID_2, Date(), Date(), Place("id1", "Fortaleza", 3.0, 34.0))
        private val PLACE_MONITOR_4 = PlaceMonitor(USER_ID_2, Date(), Date(), Place("id2", "Recife", 10.0, 34.0))
    }
}